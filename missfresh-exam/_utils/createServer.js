const http = require('http');
const fs = require('fs');

const HTTP_PORT = 3000;

function readFile(filePath) {
    return new Promise(function (resolve, reject) {
        fs.readFile(filePath, 'utf8', function (error, content) {
            if (error) {
                reject(error);
            } else {
                resolve(content);
            }
        });
    });
}

const mimeTypes = {
    html: 'text/html',
    css: 'text/css',
    jpeg: 'image/jpeg',
    jpg: 'image/jpeg',
};

function getMimeType(filePath) {
    const extension = filePath.split('.').pop();
    return mimeTypes[extension] || null;
}

function pathToRegExp(path) {
    const regexp = new RegExp('^' + path.replace(/\//g, '\\/').replace(/(<\w+>)/g, '(\\w+)') + '$');
    const paramsList = (path.match(/(<\w+>)/g) || []).map(name => name.slice(1, -1));

    return {
        test: function (url) {
            return regexp.test(url);
        },
        match: function (url) {
            const match = url.match(regexp);
            if (!match) {
                return null;
            }

            const params = {};
            paramsList.forEach(function (name, index) {
                params[name] = match[index + 1];
            });

            return params;
        },
    };
}

module.exports = function (options) {
    const layoutPath = options && options.layout;
    const layoutContent = layoutPath ? readFile(layoutPath) : Promise.resolve('{{content}}');
    const routes = [];

    const server = http.createServer(function (req, res) {
        res.renderError = function (error) {
            res.setHeader('Content-Type', 'text/html');
            res.statusCode = 500;
            res.end('<pre>' + (error && error.stack) + '</pre>');
        };

        res.renderView = function (content) {
            layoutContent
                .then(function (layout) {
                    const html = layout.replace('{{content}}', content);
                    res.setHeader('Content-Type', 'text/html');
                    res.end(html);
                })
                .catch(function (error) {
                    res.renderError(error);
                });
        };

        res.sendFile = function (filePath, contentType) {
            res.setHeader('Content-Type', contentType || 'text/html');
            fs.createReadStream(filePath).pipe(res);
        };

        try {
            const route = routes.find(route => route.test(req.url));
            if (!route) {
                res.statusCode = 404;
                res.renderView('Page not found...');
                return;
            }

            req.params = route.match(req.url);
            route.handler(req, res);
        } catch (error) {
            res.renderError(error);
        }
    });

    server.listen(HTTP_PORT, function () {
        console.log(`Server is listening on http://localhost:${HTTP_PORT}/`);
    });

    return {
        addRoute: function (path, handler) {
            const regexp = pathToRegExp(path);
            routes.push({
                test: regexp.test,
                match: regexp.match,
                handler,
            });
        },

        serveStatic: function (path, filePath, contentType) {
            if (!contentType) {
                contentType = getMimeType(filePath);
            }

            this.addRoute(path, (req, res) => {
                res.sendFile(filePath, contentType);
            });
        },
    };
};
