var https = require('https');

module.exports = function (url) {
    return new Promise(function (resolve, reject) {
        https.get(url, function (res) {
            var statusCode = res.statusCode;
            var contentType = res.headers['content-type'];

            let error;
            if (statusCode !== 200) {
                error = new Error('Request Failed.\n' +
                    `Status Code: ${statusCode}`);
            } else if (!/^application\/json/.test(contentType)) {
                error = new Error('Invalid content-type.\n' +
                    `Expected application/json but received ${contentType}`);
            }

            if (error) {
                console.error(error.message);
                // consume response data to free up memory
                res.resume();
                return;
            }

            res.setEncoding('utf8');
            var rawData = '';
            res.on('data', function (chunk) {
                rawData += chunk;
            });

            res.on('end', function () {
                try {
                    var parsedData = JSON.parse(rawData);
                    resolve(parsedData);
                } catch (error) {
                    reject(error);
                }
            });
        }).on('error', function (error) {
            reject(error);
        });
    });
};
