const path = require('path');
const { createServer, getJSON } = require('../_utils');

function getRecipe(recipeId) {
    return getJSON(`https://www.missfresh.com/en/recipe/json/${recipeId}`);
}

function getMenu() {
    return getJSON('https://www.missfresh.com/ajax/menu/custom');
}

const server = createServer({
    layout: path.join(__dirname, 'layout.html'),
});

server.addRoute('/', function (req, res) {
    getMenu()
    .then(function (menu) {
        const weeks = menu.weeks;
        res.renderView(`
            ${weeks.map(week => `
                <h2>${week.deliveryWeek}</h2>
                <div>
                <ul>
                ${week.recipes.map(recipe => `
                    <li>
                    <a href="/recipe/${recipe.id}">
                    ${recipe.titles.EN_CA}
                    </a>
                    </li>
                    `).join('')}
                </ul>
                </div>
                `).join('')}
            `);
    })
    .catch(function (error) {
        res.renderError(error);
    });
});

server.addRoute('/recipe/<id>', function (req, res) {
    const recipeId = req.params.id;
    //res.send('allo');
    getRecipe(recipeId).then(function(result){

        const recipe = result.recipe;
        var img=recipe.images[0];
        console.log(img['formats']);
        /*for (var key in img) {
          console.log(key+' >>>>>>'+img[key]);
      }*/
      res.renderView(`

        <div class="container row justify-content-center">
            <div class="card col-6 shadow p-0">

                <div class="card-body align-items-center  m-1">
                    <h3 class="card-title text-center">${recipe.title}</h3>
                    <p class="card-subtitle text-center">${recipe.shortDescription}</p>
                </div>
                <img src="${img.formats[0].url}" alt="" class="card-img-bottom mx-1 my-0 align-self-center" style="width=${img.formats[0].width};height=${img.formats[0].height}">

                <a href="/" class="card-link btn btn-outline-primary p-2 m-0">back to ...</a>
            </div> 

        </div>

        `);
      //console.log(result);
  });


});
