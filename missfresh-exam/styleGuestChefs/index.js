const path = require('path');
const { createServer } = require('../_utils');

const server = createServer();

server.serveStatic('/', path.join(__dirname, 'index.html'));
server.serveStatic('/style.css', path.join(__dirname, 'style.css'));
server.serveStatic('/images/ara.jpg', path.join(__dirname, 'images', 'ara.jpg'));
server.serveStatic('/images/danny-st-pierre.jpg', path.join(__dirname, 'images', 'danny-st-pierre.jpg'));
server.serveStatic('/images/fiodar.jpg', path.join(__dirname, 'images', 'fiodar.jpg'));
server.serveStatic('/images/guiseppe.jpg', path.join(__dirname, 'images', 'guiseppe.jpg'));
